<?php

namespace Battleships\Service;

use BattleShips;

class GameTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Game
     */
    private $game;

    public function setUp(): void
    {
        parent::setUp();

        $game = new BattleShips\Service\Game(
            new BattleShips\Model\Playground(10, 10), [
                new BattleShips\Model\Battleship('Arthas'),
                new BattleShips\Model\Destroyer('Thrall'),
                new BattleShips\Model\Destroyer('Ragnaros')
            ]
        );

        $game->init();

        $this->game = $game;
    }

    public function testInsufficientPlaygroundSpace() : void
    {
        $this->expectException(\LogicException::class);
        $this->expectExceptionMessage('Unable to initialize game because of space constraints!');

        $game = new BattleShips\Service\Game(
            new BattleShips\Model\Playground(1, 1), [
                new BattleShips\Model\Battleship('Arthas'),
                new BattleShips\Model\Destroyer('Thrall'),
                new BattleShips\Model\Destroyer('Ragnaros')
            ]
        );

        $game->init();
    }

    public function testNoHits() : void
    {
        self::assertEmpty($this->game->getHits());
    }

    public function testHitsPersistence() : void
    {
        $this->game->registerHit('A1');
        $this->game->registerHit('A2');

        self::assertEquals(['A1', 'A2'], $this->game->getHits());
    }

    public function testShowMode() : void
    {
        $this->game->registerHit('SHOW');

        self::assertTrue($this->game->isShowMode());
    }

    public function testInvalidCoordinates() : void
    {
        $message = $this->game->registerHit('11111');

        self::assertEquals('Wrong coordinates!', $message->getMessage());
        self::assertEquals('error', $message->getType());
    }

    public function testDoubleMiss() : void
    {
        $missingBlocksMap = $this->getMissingBlocks();

        $firstHit = $this->game->registerHit($missingBlocksMap[0]);
        $message  = $this->game->registerHit($missingBlocksMap[0]);

        self::assertEquals('MISS! No ship out there!', $firstHit->getMessage());
        self::assertEquals('warning', $firstHit->getType());

        self::assertEquals('Do not repeat old mistakes. ;)', $message->getMessage());
        self::assertEquals('warning', $message->getType());
    }

    public function testDoubleHit() : void
    {
        $locationsMap = $this->getShipsLocations();

        $firstHit = $this->game->registerHit($locationsMap[0]);
        $message  = $this->game->registerHit($locationsMap[0]);

        self::assertStringContainsString('badly hit!', $firstHit->getMessage());
        self::assertEquals('success', $firstHit->getType());

        self::assertStringContainsString('already hit here! Save your bullets.', $message->getMessage());
        self::assertEquals('warning', $message->getType());
    }

    public function getGameOver() : void
    {
        $locationsMap = $this->getShipsLocations();

        foreach ($locationsMap as $coordinates) {
            $this->game->registerHit($coordinates);
        }

        self::assertTrue($this->game->isOver());
    }

    /**
     * @return array
     */
    private function getShipsLocations() : array
    {
        $coordinates = [];
        foreach ($this->game->getBattleArea() as $index => $row) {
            $rowIndex = \chr(65 + $index);

            /*** @var BattleShips\Model\Block $block */
            foreach ($row as $columnIndex => $block) {
               if (!$block->hasShip()) {
                   continue;
               }

               $coordinates[] = \sprintf('%s%d', $rowIndex, $columnIndex);
            }
        }

        return $coordinates;
    }

    /**
     * @return array
     */
    private function getMissingBlocks() : array
    {
        $coordinates = [];
        foreach ($this->game->getBattleArea() as $index => $row) {
            $rowIndex = \chr(65 + $index);

            /*** @var BattleShips\Model\Block $block */
            foreach ($row as $columnIndex => $block) {
               if ($block->hasShip()) {
                   continue;
               }

               $coordinates[] = \sprintf('%s%d', $rowIndex, $columnIndex);
            }
        }

        return $coordinates;
    }
}