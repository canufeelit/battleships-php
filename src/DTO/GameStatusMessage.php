<?php

namespace BattleShips\DTO;

class GameStatusMessage
{
    /**
     * @var string
     */
    private $message;

    /**
     * @var string
     */
    private $type;

    /**
     * @param string    $message
     * @param string    $type
     */
    public function __construct(string $message, string $type)
    {
        $this->message  = $message;
        $this->type     = $type;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
}