<?php

namespace BattleShips\DTO;

class ShipLocation
{
    public const VERTICALLY_ALIGNED   = 0;
    public const HORIZONTALLY_ALIGNED = 1;

    /**
     * @var int
     */
    private $rowPosition;

    /**
     * @var int
     */
    private $columnPosition;

    /**
     * @var int
     */
    private $alignment;

    /**
     * @var int
     */
    private $shipLength;

    /**
     * @param int $row
     * @param int $column
     * @param int $length
     * @param int $alignment
     */
    public function __construct(int $row, int $column, int $length, int $alignment)
    {
        $this->rowPosition      = $row;
        $this->columnPosition   = $column;
        $this->shipLength       = $length;
        $this->alignment        = $alignment;
    }

    /**
     * @return int
     */
    public function getRowPosition(): int
    {
        return $this->rowPosition;
    }

    /**
     * @return int
     */
    public function getColumnPosition(): int
    {
        return $this->columnPosition;
    }

    /**
     * @return bool
     */
    public function isHorizontal(): bool
    {
        return self::HORIZONTALLY_ALIGNED === $this->alignment;
    }

    /**
     * @return int
     */
    public function getShipLength(): int
    {
        return $this->shipLength;
    }
}