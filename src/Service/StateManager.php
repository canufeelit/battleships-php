<?php

namespace BattleShips\Service;

use Symfony\Component\HttpFoundation\Session\SessionInterface;

class StateManager
{
    private const GAME_PERSISTENCE_KEY = 'battle:001';

    /**
     * @var SessionInterface
     */
    private $storage;

    /**
     * @var Game
     */
    private $game;

    /**
     * @param SessionInterface  $storage
     * @param Game              $game
     */
    public function __construct(SessionInterface $storage, Game $game)
    {
        $this->storage  = $storage;
        $this->game     = $game;
    }

    /**
     * @return Game
     */
    public function getGame() : Game
    {
        if ($this->storage->has(self::GAME_PERSISTENCE_KEY)) {
            return $this->storage->get(self::GAME_PERSISTENCE_KEY);
        }

        $this->game->init();

        return $this->game;
    }

    /**
     * @param Game $game
     */
    public function persistState(Game $game) : void
    {
        $this->storage->set(self::GAME_PERSISTENCE_KEY, $game);
    }

    public function resetGame() : void
    {
        $this->storage->remove(self::GAME_PERSISTENCE_KEY);
    }
}