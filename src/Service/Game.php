<?php

namespace BattleShips\Service;

use BattleShips\DTO;
use BattleShips\Model;

class Game
{
    public const MODE_SWITCH_KEYWORD    = 'show';
    public const MODE_STANDARD          = 0;
    public const MODE_SHOW              = 1;

    /**
     * @var Model\Playground
     */
    private $playground;

    /**
     * @var Model\BattleShipInterface[]
     */
    private $ships;

    /**
     * @var array
     */
    private $hits;

    /**
     * @var int
     */
    private $mode;

    /**
     * @param Model\Playground            $playground
     * @param Model\BattleShipInterface[] $ships
     */
    public function __construct(Model\Playground $playground, array $ships)
    {
        $this->playground   = $playground;
        $this->ships        = $ships;
        $this->hits         = [];
    }

    public function init() : void
    {
        $totalBlocks = $this->playground->getRows() * $this->playground->getColumns();
        $shipsBlocks = \array_reduce($this->ships, static function (?int $shipBlocks, Model\BattleShipInterface $ship) {
            return $shipBlocks + $ship->getLength();
        });

        if ($totalBlocks < $shipsBlocks) {
            throw new \LogicException('Unable to initialize game because of space constraints!');
        }

        foreach ($this->ships as $ship) {
            $this->positionShipInGame($ship);
        }
    }

    /**
     * @param string $coordinates
     *
     * @return DTO\GameStatusMessage
     */
    public function registerHit(string $coordinates) : DTO\GameStatusMessage
    {
        $this->mode = self::MODE_STANDARD;

        $this->hits[] = $coordinates;

        $normalizedCoordinates = \strtolower(\trim($coordinates));
        if ($normalizedCoordinates === self::MODE_SWITCH_KEYWORD) {

            $this->mode = self::MODE_SHOW;

            return new DTO\GameStatusMessage('Show Mode', 'info');
        }

        try {
            return $this->hit($normalizedCoordinates);
        } catch (\LogicException $e) {
            return new DTO\GameStatusMessage($e->getMessage(), 'error');
        }
    }

    /**
     * @param string $coordinates
     *
     * @return DTO\GameStatusMessage
     */
    public function hit(string $coordinates) : DTO\GameStatusMessage
    {
        [$row, $column] = $this->parseCoordinates($coordinates);

        $block = $this->playground->findBlock($row, $column);

        if (null === $block) {
            return new DTO\GameStatusMessage('Invalid coordinates specified!', 'error');
        }

        $ship = $block->getShip();

        if (null === $ship) {
            $message = ($block->getState() === Model\Block::MARK_EMPTY) ?
                'MISS! No ship out there!' :
                'Do not repeat old mistakes. ;)';

            $block->setState(Model\Block::MARK_MISS);

            return new DTO\GameStatusMessage($message, 'warning');
        }

        if ($block->getState() === Model\Block::MARK_HIT) {
            return new DTO\GameStatusMessage(\sprintf('%s already hit here! Save your bullets.', $ship->getName()), 'warning');
        }

        $block->setState(Model\Block::MARK_HIT);

        $ship->isHit();

        if ($this->isOver()) {
            return new DTO\GameStatusMessage(\sprintf('You\'ve won this game in %s hits!', \count($this->hits)), 'success');
        }

        if ($ship->isSunk()) {
            return new DTO\GameStatusMessage(\sprintf('Ship %s just sunk!!!', $ship->getName()), 'success');
        }

        return new DTO\GameStatusMessage(\sprintf('Ship %s badly hit!', $ship->getName()), 'success');
    }

    /**
     * @return bool
     */
    public function isOver() : bool
    {
        foreach ($this->ships as $ship) {
            if (!$ship->isSunk()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return array
     */
    public function getHits() : array
    {
        return $this->hits;
    }

    /**
     * @param Model\BattleShipInterface $ship
     */
    private function positionShipInGame(Model\BattleShipInterface $ship) : void
    {
        try {
            $alignment = \random_int(0, 1);
        } catch (\Exception $e) {
            $alignment = DTO\ShipLocation::VERTICALLY_ALIGNED;
        }

        do {
            $success = $this->playground->addShip($ship, $this->getRandomLocationPoint($ship, $alignment));
        } while (!$success);
    }

    /**
     * @param Model\BattleShipInterface $ship
     * @param int                       $alignment
     *
     * @return DTO\ShipLocation
     */
    private function getRandomLocationPoint(Model\BattleShipInterface $ship, int $alignment) : DTO\ShipLocation
    {
        $rowsLength     = $this->playground->getRows();
        $columnLength   = $this->playground->getColumns();

        if ($alignment === DTO\ShipLocation::HORIZONTALLY_ALIGNED) {
            try {
                $rowPosition    = \random_int(0, $rowsLength);
                $columnPosition = \random_int(0, $columnLength - $ship->getLength());
            } catch (\Exception $e) {
                $rowPosition = $columnPosition = 0;
            }

            return new DTO\ShipLocation($rowPosition, $columnPosition, $ship->getLength(), $alignment);
        }

        try {
            $rowPosition    = \random_int(0, $rowsLength - $ship->getLength());
            $columnPosition = \random_int(0, $columnLength);
        } catch (\Exception $e) {
            $rowPosition = $columnPosition = 0;
        }

        return new DTO\ShipLocation($rowPosition, $columnPosition, $ship->getLength(), $alignment);
    }

    /**
     * @return array
     */
    public function getBattleArea() : array
    {
        return $this->playground->getBattleArea();
    }

    /**
     * @return bool
     */
    public function isShowMode() : bool
    {
        return $this->mode === self::MODE_SHOW;
    }

    /**
     * @param string $coordinates
     *
     * @throws \LogicException
     *
     * @return array
     */
    private function parseCoordinates(string $coordinates) : array
    {
        if (!\preg_match('/^(?<row>[a-z])(?<column>\d+)$/', $coordinates, $matches)) {
            throw new \LogicException('Wrong coordinates!');
        }

        $row = \ord($matches['row']) - 97;

        return [$row, (int) $matches['column']];
    }
}