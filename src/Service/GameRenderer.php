<?php

namespace BattleShips\Service;

use BattleShips\DTO\GameStatusMessage;
use BattleShips\Model\Block;
use Symfony\Component\HttpFoundation;
use Twig;

class GameRenderer
{
    private const MARKER_EMPTY  = '⚪';
    private const MARKER_HIT    = '✔';
    private const MARKER_MISS   = '❌';
    private const MARKER_SHIP   = '🚣';

    /**
     * @var Twig\Environment
     */
    private $templating;

    /**
     * @param Twig\Environment $templating
     */
    public function __construct(Twig\Environment $templating)
    {
        $this->templating = $templating;
    }

    /**
     * @param Game                  $game
     * @param GameStatusMessage     $statusMessage
     *
     * @throws \Exception
     *
     * @return HttpFoundation\Response
     */
    public function render(Game $game, ?GameStatusMessage $statusMessage) : HttpFoundation\Response
    {
        $response = $this->templating->render('index.html.twig', [
            'area'          => $game->getBattleArea(),
            'labels'        => $this->getLabels($game),
            'hits'          => $game->getHits(),
            'show_mode'     => $game->isShowMode(),
            'message'       => $statusMessage,
            'markers_ship'  => self::MARKER_SHIP,
            'markers'       => $this->getMarkers(),
        ]);

        return new HttpFoundation\Response($response);
    }

    /**
     * @param Game $game
     *
     * @return array
     */
    private function getLabels(Game $game) : array
    {
        $labels = [];
        foreach ($game->getBattleArea() as $index => $blocks) {
            $labels[] = \chr(65 + $index);
        }

        return $labels;
    }

    /**
     * @return array
     */
    private function getMarkers() : array
    {
        return [
            Block::MARK_EMPTY   => self::MARKER_EMPTY,
            Block::MARK_MISS    => self::MARKER_MISS,
            Block::MARK_HIT     => self::MARKER_HIT,
        ];
    }
}