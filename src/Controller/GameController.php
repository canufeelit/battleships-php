<?php

namespace BattleShips\Controller;

use BattleShips\Service;
use Symfony\Component\HttpFoundation;

class GameController
{
    /**
     * @var Service\GameRenderer
     */
    private $renderer;

    /**
     * @var Service\StateManager
     */
    private $stateManager;

    /**
     * @param Service\GameRenderer $renderer
     * @param Service\StateManager $stateManager
     */
    public function __construct(Service\GameRenderer $renderer, Service\StateManager $stateManager)
    {
        $this->renderer     = $renderer;
        $this->stateManager = $stateManager;
    }

    /**
     * @param HttpFoundation\Request $request
     *
     * @throws \Exception
     *
     * @return HttpFoundation\Response
     */
    public function __invoke(HttpFoundation\Request $request) : HttpFoundation\Response
    {
        if ($request->query->has('reset')) {

            $this->stateManager->resetGame();

            return new HttpFoundation\RedirectResponse('./index.php');
        }

        $game = $this->stateManager->getGame();

        $statusMessage = null;
        if ($request->request->has('coordinates') && !!$request->request->get('coordinates')) {
            $statusMessage = $game->registerHit($request->request->get('coordinates'));

            $this->stateManager->persistState($game);
        }

        return $this->renderer->render($game, $statusMessage);
    }
}
