<?php

namespace BattleShips\Model;

use BattleShips\DTO;

class Playground
{
    /**
     * @var int
     */
    private $rows;

    /**
     * @var int
     */
    private $columns;

    /**
     * @var array
     */
    private $battleArea;

    /**
     * @param int $rows
     * @param int $columns
     */
    public function __construct(int $rows, int $columns)
    {
        $this->rows         = $rows;
        $this->columns      = $columns;
        $this->battleArea   = $this->buildArea();
    }

    /**
     * @return array
     */
    private function buildArea() : array
    {
        $battleArea = [];

        for ($i = 0; $i < $this->rows; ++$i) {
            for ($j = 0; $j < $this->columns; ++$j) {
                $battleArea[$i][$j] = new Block();
            }
        }

        return $battleArea;
    }

    /**
     * @return int
     */
    public function getRows(): int
    {
        return $this->rows;
    }

    /**
     * @return int
     */
    public function getColumns(): int
    {
        return $this->columns;
    }

    /**
     * @return array
     */
    public function getBattleArea(): array
    {
        return $this->battleArea;
    }

    /**
     * @param BattleShipInterface   $ship
     * @param DTO\ShipLocation      $location
     *
     * @return bool
     */
    public function addShip(BattleShipInterface $ship, DTO\ShipLocation $location) : bool
    {
        if (!$this->isLocationAvailable($location)) {
            return false;
        }

        if ($location->isHorizontal()) {
            $availableBlocks = $this->getHorizontalBlocks($location);
        } else {
            $availableBlocks = [];
            for ($i = 1; $i < $location->getShipLength(); ++$i) {
                $availableBlocks[] = $this->battleArea[$location->getRowPosition() + $i][$location->getColumnPosition()];
            }
        }

        /*** @var Block[] $availableBlocks */
        foreach ($availableBlocks as $block) {
            $block->setShip($ship);
        }

        return true;
    }

    /**
     * @param DTO\ShipLocation $location
     *
     * @return bool
     */
    private function isLocationAvailable(DTO\ShipLocation $location) : bool
    {
        if (!$this->isInside($location->getRowPosition(), $location->getColumnPosition())) {
            return false;
        }

        if ($location->isHorizontal()) {
            return 0 === \count(\array_filter($this->getHorizontalBlocks($location), static function (Block $block) {
                return $block->hasShip();
            }));
        }

        for ($i = 0; $i < $location->getShipLength(); ++$i) {
            /*** @var Block $block */
            $block = $this->battleArea[$location->getRowPosition() + $i][$location->getColumnPosition()];
            if ($block->hasShip()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param DTO\ShipLocation $location
     *
     * @return array
     */
    private function getHorizontalBlocks(DTO\ShipLocation $location) : array
    {
        return \array_slice(
            $this->battleArea[$location->getRowPosition()],
            $location->getColumnPosition(),
            $location->getShipLength()
        );
    }

    /**
     * @param int $row
     * @param int $column
     *
     * @return bool
     */
    public function isInside(int $row, int $column) : bool
    {
        return isset($this->battleArea[$row][$column]);
    }

    /**
     * @param int $row
     * @param int $column
     *
     * @return Block
     */
    public function findBlock(int $row, int $column) : ?Block
    {
        if (!$this->isInside($row, $column)) {
            return null;
        }

        return $this->battleArea[$row][$column];
    }
}