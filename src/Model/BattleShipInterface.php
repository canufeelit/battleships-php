<?php

namespace BattleShips\Model;

interface BattleShipInterface
{
    /**
     * @return int
     */
    public function getLength() : int;

    /**
     * @return string
     */
    public function getName() : string;

    /**
     * @return void
     */
    public function isHit() : void;

    /**
     * @return bool
     */
    public function isSunk() : bool;
}