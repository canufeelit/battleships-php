<?php

namespace BattleShips\Model;

class Block
{
    public const MARK_EMPTY    = 0;
    public const MARK_HIT      = 1;
    public const MARK_MISS     = 2;

    /**
     * @var int
     */
    private $state;

    /**
     * @var BattleShipInterface
     */
    private $ship;

    public function __construct()
    {
        $this->state = self::MARK_EMPTY;
    }

    /**
     * @param int $state
     *
     * @return $this
     */
    public function setState(int $state) : self
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return int
     */
    public function getState(): int
    {
        return $this->state;
    }

    /**
     * @param BattleShipInterface $ship
     *
     * @return $this
     */
    public function setShip(BattleShipInterface $ship) : self
    {
        $this->ship = $ship;

        return $this;
    }

    /**
     * @return BattleShipInterface
     */
    public function getShip(): ?BattleShipInterface
    {
        return $this->ship;
    }

    /**
     * @return bool
     */
    public function hasShip(): bool
    {
        return null !== $this->ship;
    }
}