<?php

namespace BattleShips\Model;

class Ship implements BattleShipInterface
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var int
     */
    protected $length;

    /**
     * @var int
     */
    protected $hits;

    /**
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return void
     */
    public function isHit(): void
    {
        $this->hits++;
    }

    /**
     * @return bool
     */
    public function isSunk(): bool
    {
        return $this->hits >= $this->length;
    }

    /**
     * @return int
     */
    public function getLength(): int
    {
        return $this->length;
    }
}