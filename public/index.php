<?php

require __DIR__.'/../vendor/autoload.php';

use BattleShips;
use Symfony\Component\HttpFoundation;

$request = HttpFoundation\Request::createFromGlobals();
$session = new HttpFoundation\Session\Session();
$session->start();

$request->setSession($session);

$twig = new Twig\Environment(
    new Twig\Loader\FilesystemLoader([__DIR__.'/../templates'])
);

$game = new BattleShips\Service\Game(
    new BattleShips\Model\Playground(10, 10), [
        new BattleShips\Model\Battleship('Arthas'),
        new BattleShips\Model\Destroyer('Thrall'),
        new BattleShips\Model\Destroyer('Ragnaros')
    ]
);

$controller = new BattleShips\Controller\GameController(
    new BattleShips\Service\GameRenderer($twig),
    new BattleShips\Service\StateManager($session, $game)
);

try {
    $controller($request)->send();
} catch (\Exception $e) {
    echo '<h1>Unable to complete request!</h1>';
}